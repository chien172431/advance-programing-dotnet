
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define LED1 5
#define LED2 4
#define LED3 14
#define LED4 12

const char* ssid = "TP-Link_03BC";
const char* password = "95181196";
const char* mqtt_server = "192.168.0.103";

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi(){
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
    }
  Serial.println("");
  Serial.print("WiFi connected - ESP IP address: ");
  Serial.println(WiFi.localIP());
  }
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  char messageLED[length+1];
    for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageLED[i] = (char)message[i];
  }
  Serial.println();
  
  if(topic=="status/001"){
      Serial.print("Changing LED to ");
      Serial.println(messageLED[length-2]);
      switch(messageLED[length-2]){
        case '0':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, LOW);
          break;
        case '1':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, HIGH);
          break;
        case '2':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, LOW);
          break;
        case '3':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, HIGH);
          break;
        case '4':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, LOW);
          break;
        case '5':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, HIGH);
          break;
        case '6':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, LOW);
          break;
        case '7':
          digitalWrite(LED1, LOW);   
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, HIGH);
          break;
        case '8':
          digitalWrite(LED1, HIGH);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, LOW);
          break;
        case '9':
          digitalWrite(LED1, HIGH);   
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, HIGH);
          break;
        default:
          digitalWrite(LED1, HIGH);   
          digitalWrite(LED2, HIGH);
          digitalWrite(LED3, HIGH);
          digitalWrite(LED4, HIGH);
      }   
  }
  Serial.println();
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");  
      // Subscribe or resubscribe to a topic
      // You can subscribe to more topics (to control more LEDs in this example)
      client.subscribe("status/001");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void setup() {
  // put your setup code here, to run once:
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);

  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server,1883);
  client.setCallback(callback);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (!client.connected()) {
    reconnect();
  }
  if(!client.loop()) client.connect("ESP8266Client");
 }

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Mvc;
using System.Text;
using System.Threading.Tasks;

namespace WinApp.Controllers
{
    using DEV = Models.DeviceViewModel;

    class DeviceController : BaseController
    {
        static List<DEV> _devices;
        public static List<DEV> Devices => _devices;

        public ActionResult Default()
        {
            if (_devices == null)
            {
                return Post(CreateApiContext(new { }, null, "device/select"), o => {
                    _devices = o.ToObject<List<DEV>>();

                    MqttController.Connected += (broker) => { 
                        foreach (var device in _devices)
                        {
                            if (device.Id != null)
                            {
                                broker.Subscribe(new string[] { "status/" + device.Id }, new byte[] { 1 });
                            }
                        }
                    };
                    Engine.CreateThread(MqttController.Connect);

                    GoFirst();
                });
            }
            return View(_devices);
        }

        public ActionResult Status(string id, JObject o)
        {
            foreach (var device in _devices)
            {
                if (device.Id == id)
                {
                    device.UpdateStatus((int)o.First.Last);
                    device.Changed += (d, v) => {
                                         
                    };
                    //device = new DEV { Id = id, Status = (int)o.First.Last };
                    //_selected.Changed += (d, v) => {
                    //    Publish(v);
                    //};
                }
            }
            return Done();
        }
    }
}
